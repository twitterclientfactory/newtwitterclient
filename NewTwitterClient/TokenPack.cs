﻿using CoreTweet;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewTwitterClient
{
    public class TokenPack
    {
        public Tokens Tokens { get; private set; }

        public void SetTokens(Tokens tokens)
        {
            if (tokens != null) Tokens = tokens;
        }
    }
}
