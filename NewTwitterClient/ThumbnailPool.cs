﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace NewTwitterClient
{
    static class ThumbnailPool
    {
        static Dictionary<string, Image> pool = new Dictionary<string, Image>();

        static public Image request(Uri uri)
        {
            if (!pool.ContainsKey(uri.ToString()))
                pool.Add(uri.ToString(), fetchImageFromUri(uri));
            return pool[uri.ToString()];
        }

        static Image fetchImageFromUri(Uri uri)
        {
            WebClient client = new WebClient();
            var imgData = client.DownloadData(uri);
            MemoryStream imgStream = new MemoryStream();
            imgStream.Write(imgData, 0, imgData.Length);
            return new Bitmap(imgStream);
        }

    }
}
