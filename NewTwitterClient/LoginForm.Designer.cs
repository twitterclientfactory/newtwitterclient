﻿namespace NewTwitterClient
{
    partial class LoginForm
    {
        /// <summary>
        /// 必要なデザイナー変数です。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        /// <param name="disposing">マネージ リソースが破棄される場合 true、破棄されない場合は false です。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows フォーム デザイナーで生成されたコード

        /// <summary>
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
            this.AuthButton = new System.Windows.Forms.Button();
            this.MessageBox = new System.Windows.Forms.RichTextBox();
            this.IdentifyForm = new System.Windows.Forms.TextBox();
            this.PasswordForm = new System.Windows.Forms.TextBox();
            this.IdentifyFormLabel = new System.Windows.Forms.Label();
            this.PasswordFormLabel = new System.Windows.Forms.Label();
            this.MessageBoxLabel = new System.Windows.Forms.Label();
            this.webBrowser1 = new System.Windows.Forms.WebBrowser();
            this.QuitButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // AuthButton
            // 
            this.AuthButton.Location = new System.Drawing.Point(197, 103);
            this.AuthButton.Name = "AuthButton";
            this.AuthButton.Size = new System.Drawing.Size(75, 20);
            this.AuthButton.TabIndex = 0;
            this.AuthButton.Text = "認証する";
            this.AuthButton.UseVisualStyleBackColor = true;
            this.AuthButton.Click += new System.EventHandler(this.button1_Click);
            // 
            // MessageBox
            // 
            this.MessageBox.HideSelection = false;
            this.MessageBox.Location = new System.Drawing.Point(15, 27);
            this.MessageBox.Name = "MessageBox";
            this.MessageBox.ReadOnly = true;
            this.MessageBox.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.Vertical;
            this.MessageBox.Size = new System.Drawing.Size(260, 69);
            this.MessageBox.TabIndex = 1;
            this.MessageBox.TabStop = false;
            this.MessageBox.Text = "Twitterにログインします。\nIDとパスワードを入力して認証してください。\n";
            // 
            // IdentifyForm
            // 
            this.IdentifyForm.Location = new System.Drawing.Point(92, 105);
            this.IdentifyForm.Name = "IdentifyForm";
            this.IdentifyForm.Size = new System.Drawing.Size(99, 19);
            this.IdentifyForm.TabIndex = 6;
            // 
            // PasswordForm
            // 
            this.PasswordForm.Location = new System.Drawing.Point(92, 131);
            this.PasswordForm.Name = "PasswordForm";
            this.PasswordForm.PasswordChar = ']';
            this.PasswordForm.Size = new System.Drawing.Size(99, 19);
            this.PasswordForm.TabIndex = 7;
            this.PasswordForm.UseSystemPasswordChar = true;
            // 
            // IdentifyFormLabel
            // 
            this.IdentifyFormLabel.AutoSize = true;
            this.IdentifyFormLabel.Font = new System.Drawing.Font("MS UI Gothic", 10.5F);
            this.IdentifyFormLabel.Location = new System.Drawing.Point(16, 106);
            this.IdentifyFormLabel.Name = "IdentifyFormLabel";
            this.IdentifyFormLabel.Size = new System.Drawing.Size(70, 14);
            this.IdentifyFormLabel.TabIndex = 8;
            this.IdentifyFormLabel.Text = "ID or Email";
            // 
            // PasswordFormLabel
            // 
            this.PasswordFormLabel.AutoSize = true;
            this.PasswordFormLabel.Font = new System.Drawing.Font("MS UI Gothic", 10.5F);
            this.PasswordFormLabel.Location = new System.Drawing.Point(16, 132);
            this.PasswordFormLabel.Name = "PasswordFormLabel";
            this.PasswordFormLabel.Size = new System.Drawing.Size(63, 14);
            this.PasswordFormLabel.TabIndex = 9;
            this.PasswordFormLabel.Text = "Password";
            // 
            // MessageBoxLabel
            // 
            this.MessageBoxLabel.AutoSize = true;
            this.MessageBoxLabel.Font = new System.Drawing.Font("MS UI Gothic", 10.5F);
            this.MessageBoxLabel.Location = new System.Drawing.Point(12, 7);
            this.MessageBoxLabel.Name = "MessageBoxLabel";
            this.MessageBoxLabel.Size = new System.Drawing.Size(154, 14);
            this.MessageBoxLabel.TabIndex = 10;
            this.MessageBoxLabel.Text = "認証画面からのメッセージ：";
            // 
            // webBrowser1
            // 
            this.webBrowser1.Location = new System.Drawing.Point(264, 1);
            this.webBrowser1.MinimumSize = new System.Drawing.Size(20, 20);
            this.webBrowser1.Name = "webBrowser1";
            this.webBrowser1.ScrollBarsEnabled = false;
            this.webBrowser1.Size = new System.Drawing.Size(20, 20);
            this.webBrowser1.TabIndex = 11;
            this.webBrowser1.TabStop = false;
            this.webBrowser1.Visible = false;
            this.webBrowser1.WebBrowserShortcutsEnabled = false;
            this.webBrowser1.DocumentCompleted += new System.Windows.Forms.WebBrowserDocumentCompletedEventHandler(this.webBrowser1_DocumentCompleted_1);
            // 
            // QuitButton
            // 
            this.QuitButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.QuitButton.Location = new System.Drawing.Point(197, 129);
            this.QuitButton.Name = "QuitButton";
            this.QuitButton.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.QuitButton.Size = new System.Drawing.Size(75, 20);
            this.QuitButton.TabIndex = 12;
            this.QuitButton.Text = "あきらめる";
            this.QuitButton.UseVisualStyleBackColor = true;
            this.QuitButton.Click += new System.EventHandler(this.button2_Click);
            // 
            // LoginForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 162);
            this.Controls.Add(this.QuitButton);
            this.Controls.Add(this.webBrowser1);
            this.Controls.Add(this.MessageBoxLabel);
            this.Controls.Add(this.PasswordFormLabel);
            this.Controls.Add(this.IdentifyFormLabel);
            this.Controls.Add(this.AuthButton);
            this.Controls.Add(this.PasswordForm);
            this.Controls.Add(this.IdentifyForm);
            this.Controls.Add(this.MessageBox);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "LoginForm";
            this.Text = "ログイン";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button AuthButton;
        private System.Windows.Forms.RichTextBox MessageBox;
        private System.Windows.Forms.TextBox IdentifyForm;
        private System.Windows.Forms.TextBox PasswordForm;
        private System.Windows.Forms.Label IdentifyFormLabel;
        private System.Windows.Forms.Label PasswordFormLabel;
        private System.Windows.Forms.Label MessageBoxLabel;
        private System.Windows.Forms.WebBrowser webBrowser1;
        private System.Windows.Forms.Button QuitButton;


    }
}

