﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CoreTweet;

namespace NewTwitterClient
{
    public partial class LoginForm : Form
    {
        OAuth.OAuthSession session;
        TokenPack pack;

        public LoginForm(TokenPack pack)
        {
            this.pack = pack;
            InitializeComponent();
            connectToAuth();
            this.AcceptButton = AuthButton;
        }

        void connectToAuth() {
            session = OAuth.Authorize("eJWUnMOcfA4zZ1AYTKCmueS94",
                "YLZa7OwqdVnk5w7vnEdoCCWCoGSrbHm1YvCB8SQWs0OOevoKZb");
            this.webBrowser1.Url = new System.Uri(session.AuthorizeUri.AbsoluteUri, System.UriKind.Absolute);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            MessageBox.AppendText("認証中・・・。\n");
            submitAuth();
        }

        void submitAuth()
        {
            HtmlElementCollection all = webBrowser1.Document.All;
            var idForms = all.GetElementsByName("session[username_or_email]");
            var passwdForms = all.GetElementsByName("session[password]");
            idForms[0].InnerText = IdentifyForm.Text;
            passwdForms[0].InnerText = PasswordForm.Text;
            var submit = webBrowser1.Document.GetElementById("allow");
            submit.InvokeMember("click");
        }

        bool hasStarted = false;
        private void webBrowser1_DocumentCompleted_1(object sender, WebBrowserDocumentCompletedEventArgs e)
        {
            HtmlElementCollection pin = webBrowser1.Document.GetElementsByTagName("code");
            if (pin.Count != 0)
            {
                MessageBox.AppendText("認証に成功しました。\nNewTwitterClientを準備中です。\n");
                pack.SetTokens(session.GetTokens(pin[0].OuterText));
                closeDialog();
            }
            else if (hasStarted)
                MessageBox.AppendText("認証に失敗しました。\n認証し直すか、あきらめてください。\n");
            else
                easyLogin();
            hasStarted = true;
        }

        void closeDialog()
        {
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        void easyLogin()
        {
            IdentifyForm.Text = "Iruyan_Zak";
            PasswordForm.Text = "hogehoge";
            MessageBox.AppendText("かんたん認証を試行しています。\n");
            submitAuth();
        }
    }
}
