﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NewTwitterClient
{
    static class Program
    {
        /// <summary>
        /// アプリケーションのメイン エントリ ポイントです。
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            TokenPack pack = new TokenPack();
            LoginForm f1 = new LoginForm(pack);
            DialogResult res;
            res = f1.ShowDialog();
            f1.Dispose();
            if (res == DialogResult.OK)
                Application.Run(new Form2(pack.Tokens));
        }
    }
}
