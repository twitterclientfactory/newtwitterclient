﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CoreTweet;
using System.IO;

namespace NewTwitterClient
{
    public partial class Form2 : Form
    {
        Twitter twitter;
        Status[] timeLine;
        UserResponse user;

        public Form2(Tokens twitterTokens)
        {
            InitializeComponent();
            initializeFromToken(twitterTokens);
            twitter = new Twitter(twitterTokens);
            updateTimeLine(16);
        }

        void initializeFromToken(Tokens tokens)
        {
            user = tokens.Account.VerifyCredentials();
            label1.Text = user.Name;
            label2.Text = "@" + user.ScreenName;
            linkLabel1.Text = user.StatusesCount.ToString();
            linkLabel2.Text = user.FriendsCount.ToString();
            linkLabel3.Text = user.FollowersCount.ToString();
            pictureBox2.Image = ThumbnailPool.request(user.ProfileImageUrl);
        }
        void updateTimeLine(int linesCount)
        {
            twitter.ReceiveTimeLine(linesCount);
            timeLine = twitter.GetTimeLine();
            dataGridView1.Rows.Add(linesCount);

            int ioThumbnail = dataGridView1.Columns.IndexOf(columnOfThumbnail);
            int ioName = dataGridView1.Columns.IndexOf(columnOfName);
            int ioID = dataGridView1.Columns.IndexOf(columnOfID);
            int ioTime = dataGridView1.Columns.IndexOf(columnOfTweetedAt);
            int ioBody = dataGridView1.Columns.IndexOf(columnOfTweetBody);

            for (int i = 0; i < timeLine.Count(); i++)
            {
                var s = timeLine[i];
                var r = dataGridView1.Rows[i];
                r.Cells[ioThumbnail].Value = ThumbnailPool.request(s.User.ProfileImageUrl);
                r.Cells[ioName].Value = s.User.Name;
                r.Cells[ioID].Value = s.User.ScreenName;
                r.Cells[ioTime].Value = tweetedTimeAlongFormat(s.CreatedAt);
                r.Cells[ioBody].Value = s.Text;
            }
        }
        string tweetedTimeAlongFormat(DateTimeOffset ttime)
        {
            var dt = DateTimeOffset.Now - ttime;
            if (dt.Days != 0)       return dt.Days.ToString() + "日前";
            if (dt.Hours != 0)      return dt.Hours.ToString() + "時間前";
            if (dt.Minutes != 0)    return dt.Minutes.ToString() + "分前";
            else                    return dt.Seconds.ToString() + "秒前";
        }
        private void dataGridView1_SelectionChanged(object sender, EventArgs e)
        {
            var s = dataGridView1.SelectedRows[0];
            richTextBox2.Text = s.Cells[dataGridView1.Columns.IndexOf(columnOfTweetBody)].Value.ToString();
            pictureBox3.Image = (Bitmap)s.Cells[dataGridView1.Columns.IndexOf(columnOfThumbnail)].Value;
            var media = timeLine[dataGridView1.Rows.IndexOf(dataGridView1.SelectedRows[0])].Entities.Media;
            if (media.Count() > 0)
            {
                // TODO: PictureFormに表示の依頼
                //ThumbnailPool.request(media[0].MediaUrl);
            }
            /*if (richTextBox2.Text.Contains("http://t.co/"))
            {
                var index = richTextBox2.Text.IndexOf("http://t.co/");
                string uriString = richTextBox2.Text.Substring(index);
                pictureBox1.Image = ThumbnailPool.request(new Uri(uriString));
            }*/ // このコードでツイート中の画像を取得したかった。
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                    twitter.Post(PostBox.Text);
                    PostBox.Clear();
            }
            catch(System.TimeoutException)
            {
                ProcessStatus.Text = "Process Timeout.";
            }
        }

        private void PostBox_TextChanged_1(object sender, EventArgs e)
        {
            CharCount.Text = (140 - PostBox.TextLength).ToString();
            if (PostBox.TextLength > 140)
                PostButton.Enabled = false;
            else
                PostButton.Enabled = true;
        }
    }
}
