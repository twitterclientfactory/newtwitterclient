﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CoreTweet;

namespace NewTwitterClient
{
    class Twitter
    {
        List<Status> TL;
        public Tokens Tokens { get; private set; }

        public Twitter(Tokens tokens)
        {
            TL = new List<Status>();
            Tokens = tokens;
            //ReceiveTimeLine(25);
        }
        public void ReceiveTimeLine(int line)
        {
            TL = Tokens.Statuses.HomeTimeline(count => line).ToList();
        }
        public void Post(string text)
        {
            Tokens.Statuses.Update(status => text);
        }

        public Status[] GetTimeLine()
        {
            Status[] copyTL = new Status[TL.Count];
            TL.CopyTo(copyTL);
            return copyTL;
        }
    }
}
